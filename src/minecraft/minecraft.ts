import type { GameCollection } from "../abstract/game_collection"
import type { Rule } from "../abstract/game"
import { Game } from "../abstract/game"
import { Table } from "../abstract/table"

import { ingredients } from "./ingredients.js";

const grid_size: [number, number] = [3, 3];

function id_mc_to_ingredient_index(id_mc: number): number {
	for(let i = 0; i < ingredients.length; i++)
	if(ingredients[i].id_mc == id_mc)
		return i;
	throw new ReferenceError("Non-existent id_mc " + id_mc);
}

/**
 * Get the crafting table recipe for an ingredient with a recipe.
 */
function ingredient_to_table(ingredient): Table {
	let table = Table.Empty(grid_size);
	for(let i = 0; i < grid_size[0]; i++)
	for(let j = 0; j < grid_size[1]; j++) {
		let slot: [number, number] = [i, j];
		// Note: ingredient.recipe is a one-dimensional array with
		// id_mc's (not ingredient indexes!).
		let item = ingredient.recipe[i*3+j];
		if(item <= 0) continue;
		table.set_slot(slot, id_mc_to_ingredient_index(item));
	}
	return table;
}

/**
 * Collection of Minecraft-inspired crafting tasks.
 * All games start with the same inventory.
 */
class MinecraftGames implements GameCollection {
	item_n: number
	grid_size: [number, number]
	games: Game[]

	constructor() {
		this.grid_size = [grid_size[0], grid_size[1]];
		this.item_n = ingredients.length;

		// Note: The first item in the ingredients list is "Air".
		// We will always ignore "Air".
		// Ignore everything but these items:
		let ingredients_filter = [
			"Oak Wood",
			//"Cobblestone",
			"Wooden Plank",
			"Stick",
			"Coal",
			"Torch",
			"Wooden Shovel",
		];
		let all_items = [];
		for(let i = 0; i < ingredients.length; i++)
		if(ingredients_filter.includes(ingredients[i].l_name)) {
			all_items.push(i);
		}
		console.log(all_items);

		// The starting inventory is fixed for all games
		let starting_inventory_names = [
			"Oak Wood",
			//"Cobblestone",
			"Coal",
		];
		let starting_inventory = new Array();
		for(const i of all_items)
		if(starting_inventory_names.includes(ingredients[i].l_name))
			starting_inventory.push(i);

		// Some ingredients have crafting recipes. Each of these
		// crafting recipe is a rule.
		let rules = [];
		for(const i of all_items)
		if(ingredients[i].hasOwnProperty("recipe")) {
			let table = ingredient_to_table(ingredients[i]);
			let rule = {
				table: table,
				item: i
			};
			rules.push(rule);
		}

		// Some ingredients have crafting recipes. Each of those
		// ingredients will be used to build a game.
		this.games = [];
		for(const i of all_items)
		if(ingredients[i].hasOwnProperty("recipe")) {
			let table = Table.Empty(this.grid_size);
			let game = new Game(
				[...starting_inventory],
				i,
				table,
				[...rules],
				[...all_items],
			);
			this.games.push(game);
		}
	}
}

export {
	MinecraftGames,
};
