import { Program } from "./program";
import type { Action } from "./action";
import { Place } from "./place";
import { Craft } from "./craft";
import { Slot } from "./slot";

/**
 * - yields Slots
 * - returns zero
 * - when passed a boolean returns
 **/
function* enumerate_slots(
		grid_size: [number, number]
	): Generator<Slot, number, boolean> {

	for(let i = 0; i < grid_size[0]; i++)
	for(let j = 0; j < grid_size[1]; j++)
		yield new Slot(i, j);

	return 0;
}

/**
 * - yields Actions
 * - returns zero
 * - when passed a boolean returns
 **/
function* enumerate_actions(
		items: number[],
		grid_size: [number, number]
	): Generator<Action, number, boolean> {

	for(const item of items)
	for(const slot of enumerate_slots(grid_size))
		yield new Place(item, slot);

	yield new Craft();

	return 0;
}


/**
 * - yields Programs
 * - returns zero
 * - when passed a boolean returns
 **/
function* enumerate_programs_of_len(
		program_len: number,
		items: number[],
		grid_size: [number, number],
		additional_primitives: Program[],
	): Generator<Program, number, boolean> {

	// Base case: the empty program
	if(program_len == 0) {
		let empty_program = new Program([]);
		yield empty_program;
		return 0;
	}

	// Inductive case: for each program of length n-1, append
	// all actions and all additional primitives.
	let gen = enumerate_programs_of_len(
		program_len-1,
		items,
		grid_size,
		additional_primitives,
	);
	for(const program of gen) {
		// First enumerate additional primitives, so that
		// complex programs can be explored first
		for(const program2 of additional_primitives) {
			let new_actions = [...program.actions, ...program2.actions];
			let new_program = new Program(new_actions);
			yield new_program;
		}
		for(const action of enumerate_actions(items, grid_size)) {
			let new_actions = [...program.actions, action];
			let new_program = new Program(new_actions);
			yield new_program;
		}
	}

	return 0;
}


/**
 * - yields Programs
 * - returns zero
 * - when passed a boolean the iterator returns
 **/
function* enumerate_programs(
		max_program_len: number,
		items: number[],
		grid_size: [number, number],
		additional_primitives: Program[],
	): Generator<Program, number, boolean> {
	let program_len = 1;
	while(program_len <= max_program_len) {
		let gen = enumerate_programs_of_len(
			program_len,
			items,
			grid_size,
			additional_primitives,
		);
		for(const program of gen)
			yield program
		program_len++;
	}
	return 0;
}

export {
	enumerate_actions,
	enumerate_programs,
	enumerate_programs_of_len,
};
