import type { Action } from "./action";
import type { Slot } from "./slot";

class Place implements Action {
	constructor(public item: number, public slot: Slot) { }

	to_code(): string {
		return "place(" + this.item + ", " + this.slot.to_code() + ")";
	}
}

export { Place }
