import type { Slot } from "./slot";
import type { Code } from "./code";

interface Action extends Code {
}

export type { Action }
