import type { Code } from "./code";

class Slot implements Code {
	constructor(public i: number, public j: number) {}

	to_code(): string {
		return "[" + this.i + ", " + this.j + "]";
	}
}

export { Slot };
