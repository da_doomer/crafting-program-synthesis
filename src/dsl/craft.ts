import type { Action } from "./action";
import type { Slot } from "./slot";

class Craft implements Action {
	constructor() { }

	to_code(): string {
		return "craft()";
	}
}

export { Craft }
