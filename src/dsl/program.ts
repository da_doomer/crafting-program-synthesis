import type { Action } from "./action";
import type { Code } from "./code";

class Program implements Code {
	constructor(public actions: Action[]) {}

	to_code(): string {
		let code: string = "";
		for(const action of this.actions) {
			code += action.to_code() + ";\n";
		}
		return code;
	}
}

export { Program };
