import { Table } from "./table";
import type { Program } from "../dsl/program";
import { Place } from "../dsl/place";
import { Craft } from "../dsl/craft";

type Rule = {
	table: Table;
	item: number;
}

/**
 * A crafting game.
 *
 * A game is defined by an inventory, a target item, a crafting
 * table, and some crafting rules.
 *
 * An item can be added to the inventory whenever the table matches
 * a rule which produces it, in which case the table is replaced
 * by an empty table.
 *
 * The goal in each game is to sequentially place items in the
 * crafting table so that the target item gets added to the
 * inventory. Placing an item does not remove it from the
 * inventory.
 *
 * If the starting inventory contains the target item, then no
 * steps are needed to win the game.
 */
class Game {
	constructor(
		public inventory: number[],
		public target_item: number,
		public table: Table,
		public rules: Rule[],
		public all_items: number[],
	) { }

	/**
	 * Place an item in a slot of the table. If the item is not in
	 * the inventory, it cannot be placed.
	 */
	set_slot(slot: [number, number], item: number) {
		if(!this.inventory.includes(item))
			return;
		this.table.set_slot(slot, item);
	}

	/**
	 * If the current table can be used to craft an item, then the
	 * item is added to the inventory and the tabled is replaced
	 * with an empty table.
	 *
	 * The first craftable item is crafted.
	 */
	craft() {
		for(const rule of this.rules)
		if(rule.table.congruent(this.table)) {
			this.inventory.push(rule.item);
			this.table = Table.Empty(this.table.grid_size());
			return;
		}
	}

	is_won() {
		return this.inventory.includes(this.target_item);
	}

	execute(program: Program) {
		for(const action of program.actions) {
			if(action instanceof Place) {
				let slot: [number, number] = [action.slot.i, action.slot.j];
				this.set_slot(slot, action.item);
			} else if(action instanceof Craft) {
				this.craft();
			} else {
				throw new TypeError("Semantics not implemented for this action type" + action.constructor.name);
			}
		}
	}
}

export type {
	Rule
};

export {
	Game
};
