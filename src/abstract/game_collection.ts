import type { Game } from "./game"

interface GameCollection {
	item_n: number,
	grid_size: [number, number],
	games: Game[]
}

export type { GameCollection };
