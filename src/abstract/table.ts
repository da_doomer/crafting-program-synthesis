function array_equals(s1: number[][], s2: number[][]): boolean {
	if(s1.length != s2.length)
		return false;
	if(s1.length <= 0 || s2.length <= 0 || s1[0].length != s2[0].length)
		return false;

	for(let i = 0; i < s1.length; i++)
	for(let j = 0; j < s1[0].length; j++)
		if(s1[i][j] != s2[i][j])
			return false;

	return true;
}


/**
 * A Minecraft-inspired crafting table. It is assumed all items are
 * numbers greater or equal than zero (thus, there is no "empty"
 * ingredient).
 */
class Table {
	constructor(public slots: number[][]) { }

	static Empty(
		grid_size: [number, number],
	): Table {
		let slots = new Array();
		for(let i = 0; i < grid_size[0]; i++) {
			slots[i] = new Array();
			for(let j = 0; j < grid_size[1]; j++) {
				slots[i].push(-1);
			}
		}
		return new Table(slots);
	}

	grid_size(): [number, number] {
		return [this.slots.length, this.slots[0].length];
	}

	is_clear(slot: [number, number]): boolean {
		return this.slots[slot[0]][slot[1]] === -1;
	}

	clear_slot(slot: [number, number]) {
		this.slots[slot[0]][slot[1]] = -1;
	}

	set_slot(slot: [number, number], item: number) {
		this.slots[slot[0]][slot[1]] = item;
	}

	get_slot(slot: [number, number]): number {
		return this.slots[slot[0]][slot[1]];
	}

	displaced(offset: [number, number]): Table {
		let grid_size = this.grid_size();
		let table = Table.Empty(grid_size);
		for(let i = 0; i < grid_size[0]; i++)
		for(let j = 0; j < grid_size[1]; j++) {
			let new_i = i + offset[0];
			let new_j = j + offset[1];
			let out_of_bounds = new_i >= grid_size[0] || new_j >= grid_size[1];
			if(out_of_bounds && !this.is_clear([i, j]))
				throw new RangeError("Attempt to displace by offset that causes objects to disappear");
			if(out_of_bounds)
				continue;
			table.set_slot([new_i, new_j], this.get_slot([i, j]));
		}
		return table;
	}

	/**
	 * Returns `true` if and only if `this` and `other` are
	 * "congruent modulo displacement". More precisely, if there is
	 * a displacement of `this`' table slots (i.e. a shift to the
	 * positions of the items) so that the `displaced_slots` is
	 * equal to `other.slots` as matrices.
	 */
	congruent(other: Table): boolean {
		let offsets = new Array();

		let grid_size = this.grid_size();
		for(let i = 0; i < grid_size[0]-1; i++)
		for(let j = 0; j < grid_size[1]-1; j++)
			offsets.push([i, j]);

		// displace(this) == other?
		for(let offset of offsets) {
			try {
				let displaced = this.displaced(offset).slots;
				if(array_equals(displaced, other.slots))
					return true;
			} catch(e) {
				continue;
			}
		}

		// displace(other) == this?
		for(let offset of offsets) {
			try {
				let displaced = other.displaced(offset).slots;
				if(array_equals(displaced, this.slots))
					return true;
			} catch(e) {
				continue;
			}
		}

		return false;
	}
}

export {
	Table
};
