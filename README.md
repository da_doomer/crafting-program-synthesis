# crafting-program-synthesis

Crafting of Minecraft-like recipes through library-growing.

## Live demo

A [live demo](https://da_doomer.gitlab.io/crafting-program-synthesis/) is available.

## Development instructions

To start a local server run:

```bash
npm run install
npm run dev
```

## Deployment instructions

An example Gitlab CI script ([.gitlab-ci.yml](.gitlab-ci.yml)) is provided.
